from random import randint
class Customer:
    # {account_number:xxxx, name:xxxx, holdings:xxxx}
    account = {}

    def __init__(self, name, deposit):
        self.account['account_number'] = randint(10000, 99999)
        self.account['name'] = name
        self.account['holdings'] = deposit

    def withdraw(self, amount):
        if self.account.get('holdings') >= amount:
            self.account['holdings'] -= amount
            print()
            print("The sum of {} has been withdrawn from your account balance".format(amount))
            self.balance()
        else:
            print()
            print("Insufficient Fund!!")
            self.balance()

    def deposit(self, amount):
        self.account['holdings'] += amount
        print()
        print("The sum of {} has been added to your account balance".format(amount))
        self.balance()

    def balance(self):
        print()
        print("Your current account balance is: ${} ".format(self.account['holdings']))

