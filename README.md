You need to create a simple banking system, made with python and OOP.
You need to create a bank which has a name and a database.
After that, user has the option to create a bank account or exit the program.
If user chooses to create an account, the user is prompted for a name and an initial deposit and the input is passed to the init method of Client class.
The unique account number is a 5 digit number, you may generate randomly. The account number serves as a password for the user to log into his account and do operations.
After creating the account, the user can choose to create another or to enter an existing one.
If user chooses the second option, the user is prompted to authenticate with the name and the account number. The two arguments are passed to the authentication method of the bank class and if such credentials exists in bank database, a variable called current_client is created from existing database.
After that the user can do operations with his account: withdraw, deposit, check balance or exit to main menu.

