from customer import Customer
from bank import Bank

bank = Bank()

print()

print("Welcome to {}!".format(bank.name))
print()

running = True

while running:
    print()
    print("""
    Choose an Option:
    
    1. Open New Bank Account
    2. Open Existing Bank Account
    3. Exit
    """)

    choice = int(input("1, 2 or 3: "))

    if choice == 1:
        print()
        print("To create an account, please fill in the information below:")
        customer = Customer(input("Name: "), int(input("Deposit amount: ")))
        bank.update_db(customer)
        print()
        print("Account created successfully! Your account Number is " , customer.account['account_number'])
    elif choice == 2:
        print()
        print("To access your account, please enter your credentials below:-")
        print()
        name = input("Name: ")
        account_number = int(input("Account Number: "))
        customer_obj = bank.authentication(name, account_number)
        if customer_obj:
            print()
            print("Welcome {}!".format(customer_obj.account['name']))
            acc_open = True

            while acc_open:
                print()
                print("""
                Choose an Option:
                
                1. Withdraw
                2. Deposit
                3. Balance
                4. Exit
                """)

                acc_choice = int(input("1,2,3,or 4: "))

                if acc_choice == 1:
                    print()
                    customer_obj.withdraw(int(input("Withdraw Amount: ")))
                elif acc_choice == 2:
                    print()
                    customer_obj.deposit(int(input("Deposit Amount: ")))
                elif acc_choice == 3:
                    customer_obj.balance()
                elif acc_choice == 4:
                    print()
                    print("Than You for Banking With Us!")
                    customer_obj = ''
                    acc_open = False
                else:
                    print()
                    print("Please Enter Correct Choice")

        else:
            print()
            print("Authentication Failed!")
            print("Reason: account not found")

            continue
    elif choice == 3:
        print()
        print("GoodBye!!")
        running = False